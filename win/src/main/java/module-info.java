module com.example.win {
    requires javafx.controls;
    requires javafx.fxml;
    requires javafx.web;
    requires FXTrayIcon;
    requires java.desktop;
    requires com.sun.jna;
    requires com.sun.jna.platform;


    opens com.example.win to javafx.fxml;
    exports com.example.win;
}
