package com.example.win;

import javafx.fxml.FXML;
import javafx.scene.control.Label;

public class FromBoyLetterController {
    @FXML
    private Label welcomeText;

    @FXML
    protected void onHelloButtonClick() {
        welcomeText.setText("Welcome to JavaFX Application!");
    }
}
