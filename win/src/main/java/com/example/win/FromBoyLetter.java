package com.example.win;

import com.dustinredmond.fxtrayicon.FXTrayIcon;
import javafx.application.Application;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.scene.Scene;
import javafx.scene.control.Alert;
import javafx.scene.control.Button;
import javafx.scene.control.MenuItem;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.layout.StackPane;
import javafx.stage.Stage;

import java.awt.*;
import java.awt.event.KeyEvent;
import java.io.File;
import java.io.UnsupportedEncodingException;
import java.net.MalformedURLException;
import java.net.URI;
import java.net.URL;

public class FromBoyLetter extends Application {
    public static Image icon = new Image(FromBoyLetter.class.getResourceAsStream("/images/icon.jpg"));
    public static String iconPath = FromBoyLetter.class.getResource("/images/icon.jpg").getPath();
    @Override
    public void start(Stage stage) throws Exception {

        stage.setScene(getAlertScene(stage));
        stage.getIcons().add(icon);
//        stage.setAlwaysOnTop(true);
        stage.show();
        registGloableListen();
    }
    public void registGloableListen(){
        // 创建一个 AWT 事件调度线程
        EventQueue.invokeLater(() -> {
            try {
                // 创建一个 Robot 对象用于模拟按键事件
                Robot robot = new Robot();

                // 注册全局键盘监听器
                KeyboardFocusManager.getCurrentKeyboardFocusManager().addKeyEventDispatcher(e -> {
                    if (e.getID() == KeyEvent.KEY_PRESSED) {
                        // 在这里处理按键事件
                        System.out.println("按下键盘按键：" + e.getKeyCode());

                        // 模拟按下的按键释放
                        robot.keyRelease(e.getKeyCode());
                    }
                    return false;
                });
            } catch (AWTException ex) {
                ex.printStackTrace();
            }
        });
    }
    public Scene getAlertScene(Stage stage) throws Exception{
        Button btn = new Button("点我啊");
        btn.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent actionEvent) {
                Alert alert = new Alert(Alert.AlertType.CONFIRMATION);
                alert.setTitle("来自男友的信件");
                alert.setHeaderText("男友：");
                alert.setContentText("臭宝子，你在干嘛咧？");
                alert.setGraphic(new ImageView(icon));
                Stage stage = (Stage) alert.getDialogPane().getScene().getWindow();
                stage.getIcons().add(icon);
                alert.showAndWait();
            }
        });
        tray(stage);
        Scene scene = new Scene(btn, 300, 200);
        return scene;
    }
    public Scene genKeyScene(){
        StackPane root = new StackPane();
        Scene scene = new Scene(root, 300, 250);

        scene.setOnKeyPressed(event -> {
            System.out.println("Key Pressed: " + event.getCode());
        });

        scene.setOnKeyReleased(event -> {
            System.out.println("Key Released: " + event.getCode());
        });
        return scene;
    }
    public static void main(String[] args) {
        launch();
    }

    public void tray(Stage stage) throws UnsupportedEncodingException {
        // 先判断是否支持系统托盘
        if (FXTrayIcon.isSupported()) {
            // 先添加系统托盘图标

            File imageFile = new File(iconPath);
            URI uri = imageFile.toURI();
            URL url = null;
            try {
                url = uri.toURL();
            } catch (MalformedURLException e) {
                e.printStackTrace();
            }
            // 创建系统托盘
            FXTrayIcon fxTrayIcon = new FXTrayIcon(stage, url);
            // 添加系统托盘菜单
            MenuItem openItem = new MenuItem(new String("打开"));
            fxTrayIcon.addMenuItem(openItem);
            MenuItem exitItem = new MenuItem("退出");
            fxTrayIcon.addMenuItem(exitItem);
            stage.hide();
            // 为菜单添加事件处理
            openItem.setOnAction(actionEvent -> {
                // 最大化显示stage窗口
//                stage.setIconified(false);
                stage.show();
            });
            exitItem.setOnAction(actionEvent -> {
                // 退出程序
                System.exit(0);
            });
            // 显示系统托盘
            fxTrayIcon.show();
        }else { // 如果不支持，关闭窗口直接退出程序
            System.exit(0);
        }
    }
}
